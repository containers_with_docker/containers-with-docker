FROM openjdk:23-ea-jdk-slim-bullseye

WORKDIR /app

COPY ./build/libs/docker-exercises-project-1.0-SNAPSHOT.jar /app

# create group and user
RUN groupadd -r dockerjavaapp && useradd -g dockerjavaapp dockerjavaapp

# set ownership and permission
RUN chown -R dockerjavaapp:dockerjavaapp /app

# switch user
USER dockerjavaapp

CMD ["java", "-jar", "docker-exercises-project-1.0-SNAPSHOT.jar"]
