</details>

******

<details>
<summary>Exercise 0: Clone project and create own Git repository </summary>
 <br />

**steps:**

```sh
# clone repository & change into project dir
git clone git@gitlab.com:twn-devops-bootcamp/latest/07-docker/docker-exercises.git
cd docker-exercises

# remove remote repo reference and create your own local repository
rm -rf .git
git init 
git add .
git commit -m "initial commit"

# create git repository on Gitlab and push your newly created local repository to it
git remote add origin git@gitlab.com:{gitlab-user}/{gitlab-repo}.git
git push -u origin master

# you can find the environment variables defined in src/main/java/com/example/DatabaseConfig.java file

```

</details>

******

<details>
<summary>Exercise 1: Start Mysql container </summary>
 <br />

**steps**

```sh
# start mysql container using docker
docker run -p 3306:3306 \
--name mysql \
-e MYSQL_ROOT_PASSWORD=***** \
-e MYSQL_DATABASE=team-member-projects \
-e MYSQL_USER=**** \
-e MYSQL_PASSWORD=**** \
-d mysql mysqld --default-authentication-plugin=mysql_native_password

# create java jar file
gradle build

# set env vars in Terminal for the java application (these will read in DatabaseConfig.java)
export DB_USER=****
export DB_PWD=****
export DB_SERVER=localhost
export DB_NAME=team-member-projects

# start java application
java -jar build/libs/docker-exercises-project-1.0-SNAPSHOT.jar

```

</details>

******

<details>
<summary>Exercise 2: Start Mysql GUI container </summary>
 <br />

**steps**

```sh
# Start phpmyadmin container using the official image.
docker pull phpmyadmin

docker ps # mysql container should be displayed, the java application should not be running because of port conflict

docker run --rm --name phpmyadmin -d --link mysql:db -p 8080:80 \
phpmyadmin

# Access phpmyadmin from your browser and test logging in to your Mysql database
localhost:8080

```

</details>

******

<details>
<summary>Exercise 3: Use docker-compose for Mysql and Phpmyadmin </summary>
 <br />

**steps**

```sh
# Create a docker-compose file with both containers.
# Configure a volume for your DB

version: '3'
services:
  mysql:
    image: mysql
    ports:
      - 3306:3306
    command: --default-authentication-plugin=mysql_native_password
    environment:
      - MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD}
      - MYSQL_DATABASE=${MYSQL_DATABASE}
      - MYSQL_USER=${MYSQL_USER}
      - MYSQL_PASSWORD=${MYSQL_PASSWORD}
    volumes:
      - mysql-data:/var/lib/mysql

  phpmyadmin:
    image: phpmyadmin
    ports:
      - 8080:80
    restart: always
    environment:
      - PMA_HOST=${PMA_HOST}
volumes:
  mysql-data:
    driver: local

# Test that everything works again
docker-compose -f mysql-docker-compose.yaml up

# Go to url on browser and login
localhost:8080

```

</details>

******

<details>
<summary>Exercise 4: Dockerize your Java Application </summary>
 <br />

**steps**

```sh
# Create a docker file
FROM openjdk:23-jdk-slim-bullseye
WORKDIR /app
COPY ./build/libs/docker-exercises-project-1.0-SNAPSHOT.jar /app
CMD ["java", "-jar", "docker-exercises-project-1.0-SNAPSHOT.jar"]

```

</details>

******

<details>
<summary>Exercise 5: Build and push Java Application Docker Image </summary>
 <br />

**steps**

```sh
# Create a docker hosted repository on Nexus
- Create docker-hosted repository on Nexus using the GUI
- Create a Role for users called nx-docker that can view docker-hosted repository
- Add the Role to a user
- Within the docker-hosted repository, add a port that docker client can connect to under HTTP
- For unsecured (HTTP) Domain, add the insecured value pair to the Docker Engine file 
  - Mac M1 chip update the vim ~/.docker/daemon.json rather than doing the above
  - "insecure-registries" : ["myregistrydomain.com:5000"]

# Open the port 8083 on server firewall

# Build the image locally and push to this repository arm64
docker build -t my-app:1.0 .
# For amd64 server (ubuntu server)
docker buildx build --platform linux/amd64 -t my-custom-sqlgui-amd64:1.0 .

# Re-tag and login to nexus docker repository credentials is stored in ~/.docker/config.json file on mac
docker login Domain:docker-port 
docker tag my-app:1.0 Domain:docker-port/my-app:1.0
docker push Domain:docker-port/my-app:1.0

```

</details>

******

<details>
<summary>Exercise 6: Create service user in the containers </summary>
 <br />

**steps**

```sh
# Create a Dockerfile for mysql db
FROM mysql:oraclelinux8
WORKDIR /app
# create group and user
RUN groupadd -r javaapp && useradd -g javaapp javaapp
# set ownership and permission
RUN chown -R javaapp:javaapp /app
# switch user
USER javaapp

# Create a Dockerfile for phpmyadmin gui for mysql
FROM phpmyadmin:apache
WORKDIR /app
# create group and user
RUN groupadd -r javaapp && useradd -g javaapp javaapp
# set ownership and permission
RUN chown -R javaapp:javaapp /app
# switch user
USER javaapp

# Build images for amd64 or arm64 (Macbook)
docker buildx build --platform linux/amd64 -t my-custom-sqlgui-amd64:1.0 .
docker buildx build --platform linux/amd64 -t my-custom-sql-amd64:1.0 .

# Re-tag images and push to the repository
docker tag my-custom-sqlgui-amd64:1.0 206.189.50.34:8083/my-custom-sqlgui-amd64:1.0  
docker tag my-custom-sqlgui-amd64:1.0 206.189.50.34:8083/my-custom-sql-amd64:1.0
docker push 206.189.50.34:8083/my-custom-sqlgui-amd64:1.0 

```

</details>

******

<details>
<summary>Exercise 7: Add application to docker-compose </summary>
 <br />

**steps**

```sh
# Add application to docker compose and config all needed env vars
version: '3'
services:
  my-app:
    image: 206.189.50.34:8083/new-my-app-amd64:1.8
    ports:
      - 8080:8080
    restart: always
    environment:
      - DB_USER=${DB_USER}
      - DB_PWD=${DB_PWD}
      - DB_SERVER=${DB_SERVER}
      - DB_NAME=${DB_NAME}
  mysql:
    image: 206.189.50.34:8083/my-custom-sql-amd64:1.0
    ports:
      - 3306:3306
    command: --default-authentication-plugin=mysql_native_password
    environment:
      - MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD}
      - MYSQL_DATABASE=${MYSQL_DATABASE}
      - MYSQL_USER=${MYSQL_USER}
      - MYSQL_PASSWORD=${MYSQL_PASSWORD}
    volumes:
      - mysql-data:/var/lib/mysql
    restart: always
  phpmyadmin:
    image: 206.189.50.34:8083/my-custom-sqlgui-amd64:1.0
    ports:
      - 8084:80
    restart: always
    environment:
      - PMA_HOST=${PMA_HOST}
volumes:
  mysql-data:
    driver: local

```
</details>

******

<details>
<summary>Exercise 8: Run application on server </summary>
 <br />

**steps**

```sh
# On the server login to docker repository hosted on the nexus repository
docker login http://206.189.50.34:8083

# Find credentials here after login in /root/snap/docker/2904/.docker/config.json

# Set insecure docker repository on server, because Nexus uses http
vim /var/snap/docker/current/config/daemon.json

# Remember check if it is a snap installation
sudo systemctl restart snap.docker.dockerd

# Add line for insecure resgistry if we are using http
"insecure-registries" : ["206.189.50.34:8083"]

# Update index file to user server address and rebuild application, rebuild docker image and push to repository

# Secure copy docker compose yaml file to server
scp source.yaml destination

# Set the needed environment variables for all containers in docker-compose on the server
export DB_USER=****
export DB_PWD=****
export DB_SERVER=mysql
export DB_NAME=team-member-projects

export MYSQL_ROOT_PASSWORD=****
export MYSQL_DATABASE=team-member-projects
export MYSQL_USER=****
export MYSQL_PASSWORD=****

export PMA_HOST=mysql
 
```
</details>

******

<details>
<summary>Exercise 9: Open ports </summary>
 <br />

**steps**

```sh
# On the server open port 8080 for the application 

```
</details>





